<?php

/* =============================================================================
 * Naranza Doman - Copyright (c) Andrea Davanzo - License MPL v2.0 - naranza.org
 * ========================================================================== */

declare(strict_types=1);

use bateo_test as test;

class bateo_testcase implements bateo_testcase_interface
{

  public function setup()
  {
    require_once DOMAN_DIR . '/scandir.php';
    require_once DOMAN_DIR . '/struct/stats.php';
  }

  public function teardown()
  {

  }

  static function doman_test_save_files(string $filename = null, array $signatures = []): array
  {
    static $files = [];
    if ('__CLEAR__' === $filename) {
      $files = [];
    } else {
      if (is_string($filename)) {
        $files[] = $filename;
      }
    }
    return $files;
  }

  public function t_destination_dir(test $t)
  {
    $scan_dir = DOMAN_TEST_DATA_DIR . '/mixed';
    $t->wie = [
      0 => "$scan_dir/intsum.php",
      1 => "$scan_dir/intsum_multiline.php",
      2 => "$scan_dir/multi_function.php"
    ];
    $stats = doman_struct_stats();
    doman_scandir(DOMAN_TEST_DATA_DIR, 'bateo_testcase::doman_test_save_files', $stats);
    $t->wig = self::doman_test_save_files();
    $t->pass_if($t->wie === $t->wig);
  }

  public function t_destination_file(test $t)
  {
    self::doman_test_save_files('__CLEAR__');
    $scan_dir = DOMAN_TEST_DATA_DIR . '/mixed';
    $t->wie = [
      0 => "$scan_dir/intsum.php",
    ];
    $stats = doman_struct_stats();
    doman_scandir("$scan_dir/intsum.php", 'bateo_testcase::doman_test_save_files', $stats);
    $t->wig = self::doman_test_save_files();
    $t->pass_if($t->wie === $t->wig);
  }

}
