<?php

function intsum_multiline(
  int $a,
  int $b): int
{
  return $a + $b;
}
