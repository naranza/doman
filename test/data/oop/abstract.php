<?php

abstract class myclass_abstract extends myclass_interface
{

  protected string $name;

  function hello(string $name)
  {
    echo 'Hello ' . $name;
  }

  abstract function set_name(string $name);
}
