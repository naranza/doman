<?php

/* =============================================================================
 * Naranza Doman - Copyright (c) Andrea Davanzo - License MPL v2.0 - naranza.org
 * ========================================================================== */

declare(strict_types=1);

use bateo_test as test;

class bateo_testcase implements bateo_testcase_interface
{

  public function setup()
  {
    require_once DOMAN_DIR . '/parse.php';
  }

  public function teardown()
  {

  }

  public function t_single_with_comment(test $t)
  {
    $t->wie = [
      0 => 'function intsum(int $a, int $b): int'
    ];
    $t->wig = doman_parse(DOMAN_TEST_DATA_DIR . '/mixed/intsum.php');
    $t->pass_if($t->wie === $t->wig);
  }

  public function t_multi(test $t)
  {
    $t->wie = [
      0 => 'function intsub(int $a, int $b): int',
      1 => 'function intmulti(int $a, int $b): int'
    ];
    $t->wig = doman_parse(DOMAN_TEST_DATA_DIR . '/mixed/multi_function.php');
    $t->pass_if($t->wie === $t->wig);
  }

  public function t_multiline(test $t)
  {
    $t->wie = [
      0 => 'function intsum_multiline(int $a, int $b): int'
    ];
    $t->wig = doman_parse(DOMAN_TEST_DATA_DIR . '/mixed/intsum_multiline.php');
    $t->pass_if($t->wie === $t->wig);
  }

  public function t_class(test $t)
  {
    $t->wie = [
      0 => [
        0 => 'class myclass',
        1 => 'function __construct()',
        2 => 'function hello(string $name)'
      ]
    ];
    $t->wig = doman_parse(DOMAN_TEST_DATA_DIR . '/oop/myclass.php');
    $t->pass_if($t->wie === $t->wig);
  }

  public function t_interface(test $t)
  {
    $t->wie = [
      0 => [
        0 => 'interface myclass_interface',
        1 => 'function hello(string $name)'
      ],
    ];
    $t->wig = doman_parse(DOMAN_TEST_DATA_DIR . '/oop/interface.php');
    $t->pass_if($t->wie === $t->wig);
  }

  public function t_abstract(test $t)
  {
    $t->wie = [
      0 => [
        0 => 'class myclass_abstract extends myclass_interface',
        1 => 'function hello(string $name)',
        2 => 'function set_name(string $name)'
      ],
    ];
    $t->wig = doman_parse(DOMAN_TEST_DATA_DIR . '/oop/abstract.php');
    $t->pass_if($t->wie === $t->wig);
  }

  public function t_trait(test $t)
  {
    $t->wie = [
      0 => [
        0 => 'trait myclass_trait',
        1 => 'function log(string $message)',
        2 => 'function get_log(): array'
      ],
    ];
    $t->wig = doman_parse(DOMAN_TEST_DATA_DIR . '/oop/trait.php');
    $t->pass_if($t->wie === $t->wig);
  }

}
