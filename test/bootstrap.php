<?php
/* =============================================================================
 * Naranza Fongo - Copyright (c) Andrea Davanzo - License MPL v2.0 - fongo.dev
 * ========================================================================== */

$root_dir = realpath(__DIR__ . '/..');

require $root_dir . '/src/initme.php';

define('DOMAN_TEST_DIR', $root_dir . '/test');

const DOMAN_TEST_DATA_DIR = DOMAN_TEST_DIR . '/data';
const DOMAN_TEST_FUNC_DIR = DOMAN_TEST_DIR . '/func';
