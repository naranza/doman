<?php

/* =============================================================================
 * Naranza Doman - Copyright (c) Andrea Davanzo - License MPL v2.0 - naranza.org
 * ========================================================================== */

declare(strict_types=1);

const DOMAN_DIR = __DIR__;
const DOMAN_VERSION = '2022.3';
