<?php

/* =============================================================================
 * Naranza Doman - Copyright (c) Andrea Davanzo - License MPL v2.0 - naranza.org
 * ========================================================================== */

declare(strict_types=1);

include __DIR__ . '/initme.php';
require_once DOMAN_DIR . '/struct/stats.php';
require_once DOMAN_DIR . '/scandir.php';
require_once DOMAN_DIR . '/callback/echo.php';

$arguments = $argv;
$num_arguments = $argc;
$num_scanned = 0;
$started = microtime(true);
$destination = end($arguments);
$function = 'doman_callback_echo';
$stats = doman_struct_stats();
echo sprintf("Naranza Doman - Version %s - by %s\n", DOMAN_VERSION, 'Andrea Davanzo');
echo "\n";
/* error checking */
if (1 === $num_arguments) {
  echo sprintf("Error: missing path\n");
} elseif ((!is_dir($destination) || !is_file($destination)) && !is_readable ($destination)) {
  echo sprintf("Error: destination not readable\n");

} else {
  /* execute scan */
  echo sprintf("Scanning of... %s\n", $destination);
  doman_scandir($destination, $function, $stats);
}
echo "\n";
echo sprintf("Total of scanned files: %d\n", $stats['num_file_scanned']);
echo sprintf("Total of functions found: %d\n", $stats['num_functions_found']);
echo sprintf("Total of oops found: %d\n", $stats['num_oops_found']);
echo sprintf("Total execution time: %01.5f\n", microtime(true) - $started);
