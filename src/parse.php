<?php

/* =============================================================================
 * Naranza Doman - Copyright (c) Andrea Davanzo - License MPL v2.0 - naranza.org
 * ========================================================================== */

declare(strict_types=1);

require_once DOMAN_DIR . '/trim_signature.php';

function doman_parse(string $filename): array
{
  $signatures = [];
  $source = file_get_contents($filename);
  if (is_string($source)) {
    $tokens = PhpToken::tokenize($source);
    $is_oop = false;
    $wait_semicolon = false;
    $is_function = false;
    $signature = [];
    foreach ($tokens as $token) {
      $append = isset($signature[0]);
      /* check if OOP */
      switch ($token->id) {
        case T_TRAIT:
        case T_INTERFACE:
        case T_CLASS:
          $is_oop = true;
          break;
      }
      /* wait semicolon */
      switch ($token->id) {
        case T_ABSTRACT:
        case T_INTERFACE:
          $wait_semicolon = true;
          break;
      }
      switch ($token->id) {
        case T_FUNCTION:
          $is_function = true;
          break;
      }
      /* check if close */
      switch ($token->id) {
        case T_TRAIT:
        case T_INTERFACE:
        case T_CLASS:
        case T_FUNCTION:
          if ($append) {
            $signatures[] = doman_trim_signature(implode('', $signature));
            $signature = [];
            $wait_semicolon = false;
            $is_function = false;
          }
          $signature[] = (string) $token->text;
          break;
        case 59: // ascii value for ';'
        case 123: //123 = ascii value for '{'
          if ($is_function || (!$is_function && 123 === $token->id)) {
            if ($append) {
              $signatures[] = doman_trim_signature(implode('', $signature));
              $signature = [];
              $wait_semicolon = false;
              $is_function = false;
            }
          }
          break;
        default:
          if ($append) {
            $signature[] = (string) $token->text;
          }
          break;
      }
    }
  }
  if ($is_oop) {
    return [$signatures];
  } else {
    return $signatures;
  }
}
