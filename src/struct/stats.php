<?php

/* =============================================================================
 * Naranza Doman - Copyright (c) Andrea Davanzo - License MPL v2.0 - naranza.org
 * ========================================================================== */

declare(strict_types=1);

function doman_struct_stats(): array
{
  return [
    'num_file_scanned' => 0,
    'num_functions_found' => 0,
    'num_oops_found' => 0
  ];
}
