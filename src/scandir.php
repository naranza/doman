<?php

/* =============================================================================
 * Naranza Doman - Copyright (c) Andrea Davanzo - License MPL v2.0 - naranza.org
 * ========================================================================== */

declare(strict_types=1);

require_once DOMAN_DIR . '/parse.php';

function doman_scandir(string $path, callable $function, array &$stats): void
{
  if (is_dir($path)) {
    $path = rtrim($path, DIRECTORY_SEPARATOR);
    $root = array_diff(scandir($path), ['..', '.']);
  } elseif (is_file($path)) {
    $root = [basename($path)];
    $path = dirname($path);
  } else {
    $root = [];
  }
  foreach ($root as $value) {
    $new_path = $path . DIRECTORY_SEPARATOR . $value;
    if (is_file($new_path)) {
      if (1 == preg_match('/\.php$/', $new_path)) {
        $signatures = doman_parse($new_path);
        call_user_func_array($function, [$new_path, $signatures]);
        $stats['num_file_scanned']++;
        foreach ($signatures as $signature) {
          if (is_string($signature)) {
            $stats['num_functions_found']++;
          } elseif (is_array($signature)) {
            $stats['num_oops_found']++;

          }
        }
      }
    } else {
      doman_scandir("$path/$value", $function, $stats);
    }
  }
}
