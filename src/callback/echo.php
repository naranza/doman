<?php

/* =============================================================================
 * Naranza Doman - Copyright (c) Andrea Davanzo - License MPL v2.0 - naranza.org
 * ========================================================================== */

declare(strict_types=1);

function doman_callback_echo(string $filename = null, array $signatures = []): void
{
  foreach ($signatures as $signature) {
    if (is_string($signature)) {
      echo sprintf("%s\t%s\n", $filename, $signature);
    } elseif (is_array($signature)) {
      foreach ($signature as $part) {
        echo sprintf("%s\t%s\n", $filename, $part);
      }
    }
  }
}
