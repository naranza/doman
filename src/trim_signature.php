<?php

/* =============================================================================
 * Naranza Doman - Copyright (c) Andrea Davanzo - License MPL v2.0 - naranza.org
 * ========================================================================== */

declare(strict_types=1);

function doman_trim_signature(string $signature)
{
  return trim(
    str_replace(';}', '',
    str_replace('( ', '(',
      str_replace('| ', '|',
        str_replace('  ', ' ',
          str_replace(["\r\n", "\r", "\n"], '', $signature))))));
}
